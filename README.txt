Test application for Health Catalyst by Steve Clark (monkeypatch@outlook.com)


*** General notes ***

Upon first run, a database will be generated with sample data. All people
created will have sensible data for name, address, etc. Each will have a
variable number of interests assigned to them, and a certain percentage of
people will also have an associated picture.

*** Usage ***

Search Pane:
The Get All button will return all records. To search by name, type into
the search field and hit Enter or click Search.

Select a person from the results list and click View or double click on
their name to view their info.

If Simulate Latency is checked, a noticeable delay is added to each query.

Details Pane:
Click New to create a new Person or click Edit to begin editing one
returned from searching.

Interests are managed with the Add and Delete buttons.

Image files can be browsed for with the Change Picture button.

Person details can be saved by clicking Save, or the user will be prompted
to save them if there are unsaved changes when attempting to create a
new person or navigating to a different one.

*** Libraries used ***
MVVM Light for MVVM and IoC framework
NBuilder and Faker.NET for generating fake data to populate database

All images obtained free for use without attribution from morguefile.com