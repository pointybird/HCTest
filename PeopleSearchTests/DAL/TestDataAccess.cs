﻿//—————————————————————————————————————————————————————
// <copyright file=”TestDataAccess.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HCTest.PeopleSearch.DAL;
using HCTest.PeopleSearch.Models;

namespace PeopleSearchTests.DAL
{
  /// <summary>
  /// A class to handle creation of Person entries with unique key ids without a database
  /// </summary>
  /// <seealso cref="HCTest.PeopleSearch.Models.Person" />
  public class TestPerson : Person
  {
    // Store next key Id in sequence to assign
    private static int NextId = 1;

    public void UpdateId()
    {
      // If a new entry, assign it next key in sequence
      if (Id == 0)
      {
        Id = NextId++;
      }
    }
  }

  /// <summary>
  /// Implementation of IDataAccess for testing ViewModels without database
  /// </summary>
  /// <seealso cref="HCTest.PeopleSearch.DAL.IDataAccess" />
  public class TestDataAccess : IDataAccess
  {
    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="TestDataAccess"/> class.
    /// </summary>
    public TestDataAccess()
    {
      // Create sample interests
      var interests = new List<string> { "Design", "Coding", "Testing" };
      Interests = new List<Interest>();
      foreach (var interest in interests)
      {
        Interests.Add(new Interest() { Name = interest });
      }

      // Pre-populate with sample people
      People = new List<Person>()
      {
        new TestPerson()
        {
          FirstName = "Alice",
          LastName = "Barker",
          Age = 20,
          City = "Juneau",
          State = "AK",
          Street1 = "123 Elm",
          Zip = "12345",
          Interests = new List<Interest>() { Interests[0] }
        },
        new TestPerson()
        {
          FirstName = "Charlie",
          LastName = "Dalton",
          Age = 30,
          City = "Auburn",
          State = "CA",
          Street1 = "456 Main",
          Zip = "23456-7890",
          Interests = new List<Interest>() { Interests[0], Interests[1] }
        },
        new TestPerson()
        {
          FirstName = "Ed",
          LastName = "Charles",
          Age = 40,
          City = "Orlando",
          State = "FL",
          Street1 = "101 Magnolia",
          Interests = new List<Interest>() { Interests[2] }
        },
      };

      // Assign IDs to entries (as if saved in database)
      foreach (var person in People)
      {
        (person as TestPerson).UpdateId();
      }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Deletes entry for a given Person record
    /// </summary>
    /// <param name="person">Person to delete</param>
    public void DeletePerson(Person person)
    {
      People.Remove(person);
    }

    /// <summary>
    /// Get all Interests stored in database
    /// </summary>
    /// <returns></returns>
    public ObservableCollection<Interest> GetInterests()
    {
      return new ObservableCollection<Interest>(Interests);
    }

    /// <summary>
    /// Get all Interests stored in database
    /// </summary>
    /// <param name="searchString"></param>
    /// <returns></returns>
    public ObservableCollection<Person> GetPeople(string searchString)
    {
      if (string.IsNullOrEmpty(searchString))
      {
        return new ObservableCollection<Person>(People);
      }
      else
      {
        string search = searchString.ToLower();
        return new ObservableCollection<Person>(People.Where(p => p.FirstName.ToLower().Contains(search) || p.LastName.ToLower().Contains(search)));
      }
    }

    /// <summary>
    /// Returns whether or not the data context has unsaved changes.
    /// </summary>
    /// <returns>True if unsaved changes, else false</returns>
    public bool HasChanges()
    {
      return Changes.Count > 0;
    }

    /// <summary>
    /// Force a manual flag that the Person entry has changed
    /// </summary>
    /// <param name="person">Person entry to flag</param>
    public void MarkChanged(Person person)
    {
      Changes[person] = true;
    }

    /// <summary>
    /// Rolls back any marked changes on entries.
    /// </summary>
    public void RevertChanges()
    {
      Changes.Clear();
    }

    /// <summary>
    /// Saves a Person entry. A new entry is created if the Person does not already exist in database.
    /// </summary>
    /// <param name="person">Person to save</param>
    /// <returns>Id of the associated Person record in database</returns>
    public int SavePerson(Person person)
    {
      if (!People.Contains(person))
      {
        People.Add(person);
      }

      var testPerson = person as TestPerson;
      if (testPerson != null)
      {
        testPerson.UpdateId(); // Simulate assignment of primary key
      }

      return person.Id;
    }
    #endregion

    #region Properties
    /// <summary>The saved Interest entries</summary>
    public List<Interest> Interests;

    /// <summary>The saved Person entries</summary>
    public List<Person> People;

    /// <summary>Used for manually tracking changes of records</summary>
    private Dictionary<Person, bool> Changes = new Dictionary<Person, bool>();
    #endregion
  }
}
