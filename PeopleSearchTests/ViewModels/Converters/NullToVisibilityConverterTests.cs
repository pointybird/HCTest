﻿//—————————————————————————————————————————————————————
// <copyright file=”NullToVisibilityConverterTests.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HCTest.PeopleSearch.ViewModels.Converters.Tests
{
  [TestClass()]
  public class NullToVisibilityConverterTests
  {
    [TestMethod()]
    public void NullToVisibilityConverterConvertTest()
    {
      var converter = new NullToVisibilityConverter();

      Assert.AreEqual(Visibility.Collapsed, converter.Convert(null, null, null, null));
      Assert.AreEqual(Visibility.Visible, converter.Convert(new object(), null, null, null));
    }

    [TestMethod()]
    [ExpectedException(typeof(NotImplementedException))]
    public void NullToVisibilityConverterConvertBackTest()
    {
      new NullToVisibilityConverter().ConvertBack(new object(), null, null, null);
    }
  }
}