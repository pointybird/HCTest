﻿//—————————————————————————————————————————————————————
// <copyright file=”NotNullToVisibilityConverterNotNullToVisibilityConverterTests.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HCTest.PeopleSearch.ViewModels.Converters.Tests
{
  [TestClass()]
  public class NotNullToVisibilityConverterNotNullToVisibilityConverterTests
  {
    [TestMethod()]
    public void NotNullToVisibilityConverterConvertTest()
    {
      var converter = new NotNullToVisibilityConverter();

      Assert.AreEqual(Visibility.Collapsed, converter.Convert(new object(), null, null, null));
      Assert.AreEqual(Visibility.Visible, converter.Convert(null, null, null, null));
    }

    [TestMethod()]
    [ExpectedException(typeof(NotImplementedException))]
    public void NotNullToVisibilityConverterConvertBackTest()
    {
      new NotNullToVisibilityConverter().ConvertBack(new object(), null, null, null);
    }
  }
}