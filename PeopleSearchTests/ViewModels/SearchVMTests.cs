﻿//—————————————————————————————————————————————————————
// <copyright file=”SearchVMTests.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Linq;
using System.Threading;
using HCTest.PeopleSearch.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PeopleSearchTests.DAL;

namespace HCTest.PeopleSearch.ViewModels.Tests
{
  [TestClass()]
  public class SearchVMTests
  {
    private SearchVM SearchVM;
    private TestDataAccess DataAccess;

    private const int TimeToWaitForAsyncMethod = 6000;

    [TestInitialize()]
    public void Initialize()
    {
      DataAccess = new TestDataAccess();
      SearchVM = new SearchVM(DataAccess);
    }

    [TestMethod()]
    public void SearchVMViewCommandTest()
    {
      SearchVM.SelectedItem = null;
      Assert.IsFalse(SearchVM.ViewCommand.CanExecute(null));

      SearchVM.SelectedItem = new Person();
      Assert.IsTrue(SearchVM.ViewCommand.CanExecute(null));
    }

    [TestMethod()]
    public void SearchVMSearchCommandTest()
    {
      SearchVM.SearchText = string.Empty;
      Assert.IsFalse(SearchVM.SearchCommand.CanExecute(null));

      SearchVM.SearchText = "Char";
      Assert.IsTrue(SearchVM.SearchCommand.CanExecute(null));

      // Search should return results from first or last name
      SearchVM.SearchCommand.Execute(null);
      Thread.Sleep(TimeToWaitForAsyncMethod);
      Assert.AreEqual(2, SearchVM.Results.Count);

      // Search should be insensitive
      SearchVM.SearchText = "Alice";
      SearchVM.SearchCommand.Execute(null);
      Thread.Sleep(TimeToWaitForAsyncMethod);
      Assert.AreEqual(1, SearchVM.Results.Count);

      SearchVM.SearchText = "aLiCe";
      SearchVM.SearchCommand.Execute(null);
      Thread.Sleep(TimeToWaitForAsyncMethod);
      Assert.AreEqual(1, SearchVM.Results.Count);
    }

    [TestMethod()]
    public void SearchVMGetAllCommandTest()
    {
      Assert.IsTrue(SearchVM.GetAllCommand.CanExecute(null));

      SearchVM.GetAllCommand.Execute(null);

      // Get all should return all records
      Thread.Sleep(TimeToWaitForAsyncMethod);
      Assert.AreEqual(DataAccess.People.Count, SearchVM.Results.Count);
    }

    [TestMethod()]
    public void SearchVMDeleteCommandTest()
    {
      SearchVM.SelectedItem = null;
      Assert.IsFalse(SearchVM.DeleteCommand.CanExecute(null));

      SearchVM.SelectedItem = DataAccess.People.First();
      Assert.IsTrue(SearchVM.DeleteCommand.CanExecute(null));

      int initialCount = DataAccess.People.Count;
      SearchVM.DeleteCommand.Execute(null);
      Assert.AreEqual(initialCount - 1, DataAccess.People.Count);
    }
  }
}