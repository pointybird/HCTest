﻿//—————————————————————————————————————————————————————
// <copyright file=”ViewModelLocatorTests.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using GalaSoft.MvvmLight.Ioc;
using HCTest.PeopleSearch.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PeopleSearchTests.DAL;

namespace HCTest.PeopleSearch.ViewModels.Tests
{
  [TestClass()]
  public class ViewModelLocatorTests
  {
    [TestMethod()]
    public void ViewModelLocatorTest()
    {
      var Locator = new ViewModelLocator();
      // Replace any registered DAL with our test one
      SimpleIoc.Default.Unregister<IDataAccess>();
      SimpleIoc.Default.Register<IDataAccess, TestDataAccess>();

      Assert.IsNotNull(Locator.PersonVM);
      Assert.IsNotNull(Locator.SearchVM);
      Assert.IsNotNull(Locator.InterestDialogVM);
    }
  }
}