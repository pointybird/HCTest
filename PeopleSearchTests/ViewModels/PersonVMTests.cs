﻿//—————————————————————————————————————————————————————
// <copyright file=”PersonVMTests.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using HCTest.PeopleSearch.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PeopleSearchTests.DAL;

namespace HCTest.PeopleSearch.ViewModels.Tests
{
  [TestClass()]
  public class PersonVMTests
  {
    private PersonVM PersonVM;
    private TestDataAccess DataAccess;

    private Person CreatePerson()
    {
      var person = new TestPerson();
      person.UpdateId();

      return person;
    }

    [TestInitialize()]
    public void Initialize()
    {
      DataAccess = new TestDataAccess();
      PersonVM = new PersonVM(DataAccess);
    }

    [TestMethod()]
    public void PersonVMEditCommandTest()
    {
      PersonVM.Person = null;
      Assert.IsFalse(PersonVM.EditCommand.CanExecute(null));

      PersonVM.Person = CreatePerson();
      Assert.IsTrue(PersonVM.EditCommand.CanExecute(null));

      Assert.IsFalse(PersonVM.EditMode);

      PersonVM.EditCommand.Execute(null);
      Assert.IsTrue(PersonVM.EditMode);
    }

    [TestMethod()]
    public void PersonVMNewCommandTest()
    {
      // Command always available
      Assert.IsTrue(PersonVM.NewCommand.CanExecute(null));

      PersonVM.Person = null;

      PersonVM.NewCommand.Execute(null);

      Assert.IsNotNull(PersonVM.Person);
      Assert.IsTrue(PersonVM.EditMode);
    }

    [TestMethod()]
    public void PersonVMSaveCommandTest()
    {
      PersonVM.Person = null;
      Assert.IsFalse(PersonVM.SaveCommand.CanExecute(null));

      PersonVM.Person = CreatePerson();
      DataAccess.MarkChanged(PersonVM.Person);
      Assert.IsTrue(PersonVM.SaveCommand.CanExecute(null));

      int initialCount = DataAccess.People.Count;
      PersonVM.SaveCommand.Execute(null);
      Assert.AreEqual(initialCount + 1, DataAccess.People.Count);
    }

    [TestMethod()]
    public void PersonVMEditPictureCommandTest()
    {
      Assert.IsFalse(PersonVM.EditPictureCommand.CanExecute(null));

      PersonVM.Person = CreatePerson();
      PersonVM.EditPerson();
      Assert.IsTrue(PersonVM.EditPictureCommand.CanExecute(null));
    }

    [TestMethod()]
    public void PersonVMAddInterestCommandTest()
    {
      Assert.IsFalse(PersonVM.AddInterestCommand.CanExecute(null));

      PersonVM.Person = CreatePerson();
      PersonVM.EditPerson();
      Assert.IsTrue(PersonVM.AddInterestCommand.CanExecute(null));
    }

    [TestMethod()]
    public void PersonVMDeleteInterestCommandTest()
    {
      Assert.IsFalse(PersonVM.DeleteInterestCommand.CanExecute(null));

      var person = DataAccess.People.First(p => p.Interests.Count > 1);

      PersonVM.UpdateForNewPerson(person);
      PersonVM.SelectedInterest = person.Interests.First();
      PersonVM.EditPerson();

      Assert.IsTrue(PersonVM.DeleteInterestCommand.CanExecute(null));
    }

    [TestMethod()]
    public void PersonVMDeleteMessageTest()
    {
      var person = DataAccess.People.First();

      PersonVM.UpdateForNewPerson(person);
      Assert.IsNotNull(PersonVM.Person);

      // Send a message that the person has been deleted
      Messenger.Default.Send(new DeleteMessage(person));
      // PersonVM should update if the person being deleted is the active one
      Assert.IsNull(PersonVM.Person);

      PersonVM.UpdateForNewPerson(CreatePerson());
      Assert.IsNotNull(PersonVM.Person);

      Messenger.Default.Send(new DeleteMessage(person));
      // PersonVM should not update if the person being deleted is not the active one
      Assert.IsNotNull(PersonVM.Person);
    }
  }
}