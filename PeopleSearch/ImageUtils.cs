﻿//—————————————————————————————————————————————————————
// <copyright file=”ImageUtils.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HCTest.PeopleSearch
{
  /// <summary>
  /// Methods for working with images for display and saving to database
  /// </summary>
  public class ImageUtils
  {
    /// <summary>Converts raw bytes into an ImageSource for display</summary>
    /// <param name="data">Image data</param>
    /// <returns>Bitmap image source for display</returns>
    public static ImageSource BitmapFromBytes(byte[] data)
    {
      if (data != null && data.Length > 0)
      {
        var image = new BitmapImage();
        using (var mem = new MemoryStream(data))
        {
          mem.Position = 0;
          image.BeginInit();
          image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
          image.CacheOption = BitmapCacheOption.OnLoad;
          image.UriSource = null;
          image.StreamSource = mem;
          image.EndInit();
        }
        image.Freeze();
        return image;
      }

      return null;
    }

    /// <summary>Converts image at given URI to bytes for storage in database</summary>
    /// <param name="uri">URI pointing to an image</param>
    /// <returns>Byte data for image encoded as Jpeg</returns>
    public static byte[] ImageBytesFromUri(Uri uri)
    {
      byte[] bytes = null;

      var image = new BitmapImage(uri);
      JpegBitmapEncoder encoder = new JpegBitmapEncoder();
      encoder.Frames.Add(BitmapFrame.Create(image));
      using (MemoryStream ms = new MemoryStream())
      {
        encoder.Save(ms);
        bytes = ms.ToArray();
      }

      return bytes;
    }
  }
}
