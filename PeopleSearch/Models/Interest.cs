﻿//—————————————————————————————————————————————————————
// <copyright file=”Interest.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HCTest.PeopleSearch.Models
{
  /// <summary>
  /// Model class for a person's interests
  /// </summary>
  public class Interest
  {
    #region Properties
    /// <summary>Primary key in database. Value is 0 if entry not yet saved.</summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; private set; }
    /// <summary>Name of the interest</summary>
    public string Name { get; set; }
    /// <summary>List of entries for people that have this interest</summary>
    public List<Person> People { get; set; }
    #endregion

    #region Methods
    /// <summary>
    /// Returns a <see cref="System.String" /> that represents this instance.
    /// </summary>
    public override string ToString()
    {
      return Name;
    }
    #endregion
  }
}
