﻿//—————————————————————————————————————————————————————
// <copyright file=”Person.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HCTest.PeopleSearch.Models
{
  /// <summary>
  /// Model class for a person
  /// </summary>
  public class Person
  {
    #region Properties
    /// <summary>Primary key in database. Value is 0 if entry not yet saved.</summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }
    /// <summary>Gets or sets the first name.</summary>
    public string FirstName { get; set; }
    /// <summary>Gets or sets the last name.</summary>
    public string LastName { get; set; }
    /// <summary>Gets or sets the first part of street address.</summary>
    public string Street1 { get; set; }
    /// <summary>Gets or sets the second part of street address.</summary>
    public string Street2 { get; set; }
    /// <summary>Gets or sets the city.</summary>
    public string City { get; set; }
    /// <summary>Gets or sets the state.</summary>
    public string State { get; set; }
    /// <summary>Gets or sets the zip code.</summary>
    public string Zip { get; set; }
    /// <summary>Gets or sets the age.</summary>
    public int Age { get; set; }
    /// <summary>Gets or sets the image.</summary>
    public byte[] Image { get; set; }
    /// <summary>Gets or sets the list of the person's interests.</summary>
    public List<Interest> Interests { get; set; }
    #endregion
  }
}
