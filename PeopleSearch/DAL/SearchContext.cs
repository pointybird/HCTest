//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�
// <copyright file=粘earchContext.cs�>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//覧覧覧覧覧覧覧覧覧覧覧蘭覧覧覧覧覧覧覧覧覧覧覧覧覧覧�

using System.Data.Entity;
using System.Linq;
using HCTest.PeopleSearch.Models;
using HCTest.PeopleSearch.Views;

namespace HCTest.PeopleSearch.DAL
{
  /// <summary>Implementation of db context for app</summary>
  /// <seealso cref="System.Data.Entity.DbContext" />
  public class SearchContext : DbContext
  {
    #region Statuc constructor
    static SearchContext()
    {
      // Set database to populate with random Person data upon creation
      Database.SetInitializer(new PersonInitializer());
    }
    #endregion

    #region Constructor
    private SearchContext()
        : base("name=SearchContext")
    {
    }
    #endregion

    #region Methods
    /// <summary>Initializes the context for use and creates the backing database if needed</summary>
    /// <returns>The initialized context</returns>
    public static SearchContext Initialize()
    {
      var context = new SearchContext();

      if (!context.Database.Exists())
      {
        // Initial database creation can take a while. Alert the user about what is happening.
        var dlg = new InitDBDialog();
        dlg.Show();

        context.Database.Initialize(true);

        dlg.Close();
      }

      // Access any element to trigger initialization at startup (as opposed to upon first use by the app)
      var forceLoad = context.Persons.FirstOrDefault();

      return context;
    }

    /// <summary>
    /// This method is called when the model for a derived context has been initialized, but
    /// before the model has been locked down and used to initialize the context.  The default
    /// implementation of this method does nothing, but it can be overridden in a derived class
    /// such that the model can be further configured before it is locked down.
    /// </summary>
    /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
    /// <remarks>
    /// Typically, this method is called only once when the first instance of a derived context
    /// is created.  The model for that context is then cached and is for all further instances of
    /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
    /// property on the given ModelBuidler, but note that this can seriously degrade performance.
    /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
    /// classes directly.
    /// </remarks>
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      // Create bridge table for many-to-many Person to Interest relationship
      modelBuilder.Entity<Person>()
              .HasMany(p => p.Interests)
              .WithMany(i => i.People)
              .Map(bridge =>
              {
                bridge.ToTable("PersonsInterests");
                bridge.MapLeftKey("PersonId");
                bridge.MapRightKey("InterestId");
              });

      base.OnModelCreating(modelBuilder);
    }
    #endregion

    #region Properties
    /// <summary>Accessor for all Person entries</summary>
    public virtual DbSet<Person> Persons { get; set; }

    /// <summaryAccessor for all Interest entries</summary>
    public virtual DbSet<Interest> Interests { get; set; }
    #endregion
  }
}