﻿//—————————————————————————————————————————————————————
// <copyright file=”DataAccess.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using HCTest.PeopleSearch.Models;

namespace HCTest.PeopleSearch.DAL
{
  /// <summary>Concrete implementation of Data Access methods</summary>
  /// <seealso cref="HCTest.PeopleSearch.DAL.IDataAccess" />
  public class DataAccess : IDataAccess
  {
    private SearchContext m_Context;

    #region Constructor
    public DataAccess()
    {
      m_Context = SearchContext.Initialize();
    }
    #endregion

    #region Methods
    /// <summary>
    /// Get all Interests stored in database
    /// </summary>
    public ObservableCollection<Interest> GetInterests()
    {
      var results = new ObservableCollection<Interest>();

      var interests = m_Context.Interests;
      foreach (var interest in interests)
      {
        // Force load of people from bridge table
        m_Context.Entry(interest).Collection(p => p.People).Load();
        results.Add(interest);
      }

      return results;
    }

    /// <summary>
    /// Force a manual flag that the Person entry has changed
    /// </summary>
    /// <param name="person">Person entry to flag</param>
    public void MarkChanged(Person person)
    {
      m_Context.Entry(person).State = EntityState.Modified;
    }

    /// <summary>
    /// Returns whether or not the data context has unsaved changes.
    /// </summary>
    /// <returns>True if unsaved changes, else false</returns>
    public bool HasChanges()
    {
      return m_Context.ChangeTracker.Entries()
        .Where(e => e.State != EntityState.Unchanged)
        .Count() > 0;
    }

    /// <summary>
    /// Rolls back any marked changes on entries. 
    /// </summary>
    public void RevertChanges()
    {
      var changed = m_Context.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged);

      foreach (var entry in changed)
      {
        switch (entry.State)
        {
          case EntityState.Modified:
            entry.CurrentValues.SetValues(entry.OriginalValues);
            entry.State = EntityState.Unchanged;
            break;

          case EntityState.Added:
            entry.State = EntityState.Detached;
            break;

          case EntityState.Deleted:
            entry.State = EntityState.Unchanged;
            break;
        }
      }
    }

    /// <summary>
    /// Returns all Person entries matching the given search string (case-insensitive).
    /// </summary>
    /// <param name="searchString">String to match against first and last names</param>
    /// <returns>All Person entries with names matching the search string. All records are returned if string is null or empty.</returns>
    public ObservableCollection<Person> GetPeople(string searchString = null)
    {
      string search = searchString.ToLower();

      IOrderedQueryable<Person> people;
      if (string.IsNullOrEmpty(searchString))
      {
        people = m_Context.Persons.OrderBy(p => p.LastName);
      }
      else
      {
        people = m_Context.Persons
            .Where(p => p.LastName.ToLower().Contains(search) || p.FirstName.ToLower().Contains(search))
            .OrderBy(p => p.LastName);
      }

      var results = new ObservableCollection<Person>();

      foreach (var person in people)
      {
        // Force load of interests from bridge table
        m_Context.Entry(person).Collection(p => p.Interests).Load();
        results.Add(person);
      }

      return results;
    }

    /// <summary>
    /// Saves a Person entry. A new entry is created if the Person does not already exist in database.
    /// </summary>
    /// <param name="person">Person to save</param>
    /// <returns>Id of the associated Person record in database</returns>
    public int SavePerson(Person person)
    {
      // Id of 0 indicates new entry; add it to list
      if (person.Id == 0)
      {
        m_Context.Persons.Add(person);
      }
      m_Context.SaveChanges();
      return person.Id; // If this was a new entry that was saved, Id will now be non-zero
    }

    /// <summary>
    /// Deletes entry for a given Person record
    /// </summary>
    /// <param name="person">Person to delete</param>
    public void DeletePerson(Person person)
    {
      if (person != null)
      {
        m_Context.Persons.Remove(person);
        m_Context.SaveChanges();
      }
    }
    #endregion
  }
}
