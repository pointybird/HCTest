﻿//—————————————————————————————————————————————————————
// <copyright file=”DataAccess.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.ObjectModel;
using HCTest.PeopleSearch.Models;

namespace HCTest.PeopleSearch.DAL
{
  /// <summary>
  /// Interface for performing data access.
  /// </summary>
  public interface IDataAccess
  {
    /// <summary>
    /// Returns whether or not the data context has unsaved changes.
    /// </summary>
    /// <returns>True if unsaved changes, else false</returns>
    bool HasChanges();
    /// <summary>
    /// Rolls back any marked changes on entries. 
    /// </summary>
    void RevertChanges();

    /// <summary>
    /// Get all Interests stored in database
    /// </summary>
    ObservableCollection<Person> GetPeople(string searchString);
    /// <summary>
    /// Force a manual flag that the Person entry has changed
    /// </summary>
    /// <param name="person">Person entry to flag</param>
    void MarkChanged(Person person);
    /// <summary>
    /// Saves a Person entry. A new entry is created if the Person does not already exist in database.
    /// </summary>
    /// <param name="person">Person to save</param>
    /// <returns>Id of the associated Person record in database</returns>
    int SavePerson(Person person);
    /// <summary>
    /// Deletes entry for a given Person record
    /// </summary>
    /// <param name="person">Person to delete</param>
    void DeletePerson(Person person);

    /// <summary>
    /// Get all Interests stored in database
    /// </summary>
    ObservableCollection<Interest> GetInterests();
  }
}
