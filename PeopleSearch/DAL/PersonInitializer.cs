﻿//—————————————————————————————————————————————————————
// <copyright file=”PersonInitializer.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FizzWare.NBuilder;
using HCTest.PeopleSearch.Models;

namespace HCTest.PeopleSearch.DAL
{
  /// <summary>
  /// Initializer for setting up default database
  /// </summary>
  /// <seealso cref="System.Data.Entity.DropCreateDatabaseIfModelChanges{HCTest.PeopleSearch.DAL.SearchContext}" />
  public class PersonInitializer : DropCreateDatabaseIfModelChanges<SearchContext>
  {
    private static int MaxRandomInterests = 5; // Max number of auto-generated interests per person
    private static int PercentageOfUsersWithPictures = 75; // Percentage of entries to be randomly assigned a picture


    /// <summary>Generates a list of random interests for a person.</summary>
    /// <param name="interests">Array of interests to choose from</param>
    /// <returns>List of 0 to MaxRandomInterests interests</returns>
    private static List<Interest> GenerateInterests(Interest[] interests)
    {
      var list = new List<Interest>();

      if (interests == null || interests.Length == 0)
      {
        return list;
      }

      for (int i = 0; i < MaxRandomInterests; i++)
      {
        if (Faker.RandomNumber.Next() % 2 == 0)
        {
          var interest = interests[Faker.RandomNumber.Next(0, interests.Length)];
          if (!list.Contains(interest))
          {
            list.Add(interest);
          }
        }
      }

      return list;
    }

    /// <summary>Selects a random test image to associate with a person entry</summary>
    /// <returns>Image data for a randomly selected image</returns>
    private static byte[] RandomImage()
    {
      return ImageUtils.ImageBytesFromUri(new Uri(string.Format("pack://application:,,,/Assets/image{0}.jpg", Faker.RandomNumber.Next(1, 16)), UriKind.RelativeOrAbsolute));
    }

    /// <summary>Seed the database with data</summary>
    /// <param name="context">The context to seed.</param>
    protected override void Seed(SearchContext context)
    {
      // Create list of interests
      var interests = new List<Interest>();
      foreach (var interest in new string[] {
            "Quilting", "Reading", "Surfing", "Hiking", "Movies",
            "Television", "Theater", "Sports", "Video Games", "Running",
            "Animals", "Writing", "Art", "Cooking", "Collectibles"
        })
      {
        context.Interests.Add(new Interest() { Name = interest });
      }
      context.SaveChanges();

      // Create data for people
      var interestArray = context.Interests.ToArray();
      var people = Builder<Person>.CreateListOfSize(100)
      .All()
        // Create realistic data for all fields
        .With(p => p.FirstName = Faker.Name.First())
        .With(p => p.LastName = Faker.Name.Last())
        .With(p => p.Age = Faker.RandomNumber.Next(18, 80))
        .With(p => p.Street1 = Faker.Address.StreetAddress())
        .With(p => p.City = Faker.Address.City())
        .With(p => p.State = Faker.Address.UsStateAbbr())
        .With(p => p.Zip = Faker.Address.ZipCode())
        // Create random amount of interests for each person
        .With(p => p.Interests = GenerateInterests(interestArray))
        // Give pictures to some percentage of the people in database
        .Random(PercentageOfUsersWithPictures)
          .With(p => p.Image = RandomImage())
      .Build();

      context.Persons.AddOrUpdate(p => p.Id, people.ToArray());
    }
  }
}
