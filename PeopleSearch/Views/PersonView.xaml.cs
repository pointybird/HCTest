﻿//—————————————————————————————————————————————————————
// <copyright file=”PersonView.xaml.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Windows.Controls;

namespace HCTest.PeopleSearch.Views
{
  /// <summary>
  /// Interaction logic for PersonView.xaml
  /// </summary>
  public partial class PersonView : UserControl
  {
    #region Constructor
    public PersonView()
    {
      InitializeComponent();
    }
    #endregion
  }
}
