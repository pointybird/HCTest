﻿//—————————————————————————————————————————————————————
// <copyright file=”MainWindow.xaml.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Windows;

namespace PeopleSearch
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    #region Constructor
    public MainWindow()
    {
      InitializeComponent();
      // Set default style from App.xaml
      Style = FindResource(typeof(Window)) as Style;
    }
    #endregion
  }
}
