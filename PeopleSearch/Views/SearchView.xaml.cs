﻿//—————————————————————————————————————————————————————
// <copyright file=”SearchView.xaml.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Windows.Controls;
using System.Windows.Input;
using HCTest.PeopleSearch.ViewModels;

namespace HCTest.PeopleSearch.Views
{
  /// <summary>
  /// Interaction logic for SearchView.xaml
  /// </summary>
  public partial class SearchView : UserControl
  {
    #region Constructor
    public SearchView()
    {
      InitializeComponent();
    }
    #endregion

    #region Properties
    public SearchVM ViewModel
    {
      get { return DataContext as SearchVM; }
    }
    #endregion

    #region Event Handlers
    /// <summary>
    /// Called to execute View command if user double clicks a Person entry in results list
    /// </summary>
    private void OnResultsDoubleClick(object sender, MouseButtonEventArgs e)
    {
      ViewModel.ViewCommand.Execute(null);
    }

    /// <summary>
    /// Called to update SearchText as user types and to execute command if Enter is pressed.
    /// </summary>
    private void OnSearchTextKeyUp(object sender, KeyEventArgs e)
    {
      ViewModel.SearchText = (sender as TextBox).Text;
      if (e.Key == Key.Enter)
      {
        ViewModel.SearchCommand.Execute(null);
      }
    }
    #endregion
  }
}
