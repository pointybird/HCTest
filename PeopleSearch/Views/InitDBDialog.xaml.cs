﻿//—————————————————————————————————————————————————————
// <copyright file=”InitDBDialog.xaml.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Windows;

namespace HCTest.PeopleSearch.Views
{
  /// <summary>
  /// Interaction logic for InitDBDialog.xaml
  /// </summary>
  public partial class InitDBDialog : Window
  {
    #region Constructor
    public InitDBDialog()
    {
      InitializeComponent();
      // Set default style from App.xaml
      Style = FindResource(typeof(Window)) as Style;
    }
    #endregion
  }
}
