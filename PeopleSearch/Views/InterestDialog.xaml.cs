﻿//—————————————————————————————————————————————————————
// <copyright file=”InterestDialog.xaml.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Windows;
using HCTest.PeopleSearch.Models;
using HCTest.PeopleSearch.ViewModels;

namespace HCTest.PeopleSearch.Views
{
  /// <summary>
  /// Interaction logic for InputDialog.xaml
  /// </summary>
  public partial class InterestDialog : Window
  {
    #region Constructor
    public InterestDialog()
    {
      InitializeComponent();
      // Set default style from App.xaml
      Style = FindResource(typeof(Window)) as Style;
    }
    #endregion

    #region Event Handlers
    private void OnOK(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
      Close();
    }
    #endregion

    #region Methods
    /// <summary>Gets the selection made by the user in the dialog</summary>
    /// <returns>The selected Interest</returns>
    public Interest GetSelection()
    {
      var vm = DataContext as InterestDialogVM;
      return vm == null ? null : vm.SelectedInterest;
    }
    #endregion
  }
}
