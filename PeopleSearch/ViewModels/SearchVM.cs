﻿//—————————————————————————————————————————————————————
// <copyright file=”SearchVM.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HCTest.PeopleSearch.DAL;
using HCTest.PeopleSearch.Models;

namespace HCTest.PeopleSearch.ViewModels
{
  /// <summary>
  /// ViewModel for Search view
  /// </summary>
  /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
  public class SearchVM : ViewModelBase
  {
    private IDataAccess m_DataAccess;

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="SearchVM"/> class.
    /// </summary>
    /// <param name="dataAccess">The data access provider to use</param>
    public SearchVM(IDataAccess dataAccess)
    {
      m_DataAccess = dataAccess;
    }
    #endregion

    #region Properties
    private bool m_SimulateLatency = false;
    /// <summary>
    /// Value indicating if artificial delay should be added to time to respond to a query
    /// </summary>
    public bool SimulateLatency
    {
      get { return m_SimulateLatency; }
      set
      {
        if (m_SimulateLatency != value)
        {
          m_SimulateLatency = value;
          RaisePropertyChanged("SimulateLatency");
        }
      }
    }

    private bool m_Loading;
    /// <summary>
    /// Indicates that VM is busy retrieving results for a query
    /// </summary>
    public bool Loading
    {
      get { return m_Loading; }
      set
      {
        if (m_Loading != value)
        {
          m_Loading = value;
          RaisePropertyChanged("Loading");
        }
      }
    }

    private ObservableCollection<Person> m_Results;
    /// <summary>Person records returned by latest query</summary>
    public ObservableCollection<Person> Results
    {
      get { return m_Results ?? (m_Results = new ObservableCollection<Person>()); }
      set
      {
        m_Results = value;
        RaisePropertyChanged("Results");
      }
    }

    private string m_SearchText;
    /// <summary>Text to match against Person names for search</summary>
    public string SearchText
    {
      get { return m_SearchText; }
      set
      {
        if (m_SearchText != value)
        {
          m_SearchText = value;
          RaisePropertyChanged("SearchText");
        }
      }
    }

    private Person m_SelectedItem;
    /// <summary>Person currently selected in the view</summary>
    public Person SelectedItem
    {
      get { return m_SelectedItem; }
      set
      {
        if (m_SelectedItem != value)
        {
          m_SelectedItem = value;
          RaisePropertyChanged("SelectedItem");
        }
      }
    }
    #endregion

    #region Commands

    #region View command
    private ICommand m_ViewCommand;
    /// <summary>Command to view data for the currently selected Person</summary>
    public ICommand ViewCommand
    {
      get
      {
        return m_ViewCommand ?? (m_ViewCommand = new RelayCommand(ViewPerson, CanViewPerson));
      }
    }

    private bool CanViewPerson()
    {
      return SelectedItem != null;
    }

    private void ViewPerson()
    {
      Messenger.Default.Send(SelectedItem);
    }
    #endregion

    #region Delete command
    private ICommand m_DeleteCommand;
    /// <summary>Command to delete the currently selected Person</summary>
    public ICommand DeleteCommand
    {
      get { return m_DeleteCommand ?? (m_DeleteCommand = new RelayCommand(DeletePerson, CanDeletePerson)); }
    }

    private bool CanDeletePerson()
    {
      return SelectedItem != null;
    }

    private void DeletePerson()
    {
      var person = SelectedItem;

      // Send message to Person view telling it to update its UI if needed
      Messenger.Default.Send(new DeleteMessage(person));

      // Remove from results in this view
      Results.Remove(person);

      // Remove from database
      m_DataAccess.DeletePerson(person);
    }
    #endregion

    #region Search command
    private ICommand m_SearchCommand;
    /// <summary>Command to search for Person entries by first or last name</summary>
    public ICommand SearchCommand
    {
      get { return m_SearchCommand ?? (m_SearchCommand = new RelayCommand(Search, CanSearch)); }
    }

    private bool CanSearch()
    {
      return !string.IsNullOrEmpty(SearchText);
    }

    /// <summary>Perform async search for Person records</summary>
    private async void Search()
    {
      Results.Clear();

      if (m_DataAccess != null)
      {
        Loading = true;

        Mouse.OverrideCursor = Cursors.Wait;

        await Task.Run(() =>
        {
          // Add artificial delay if requested by user
          if (SimulateLatency)
          {
            Thread.Sleep(new Random().Next(1000, 5000));
          }
          Results = m_DataAccess.GetPeople(SearchText);
        });

        Mouse.OverrideCursor = null;

        Loading = false;
      }

      RaisePropertyChanged("Results");
    }
    #endregion

    #region Get All command
    private ICommand m_GetAllCommand;
    /// <summary>Command to return all Person records regardless of name</summary>
    public ICommand GetAllCommand
    {
      get { return m_GetAllCommand ?? (m_GetAllCommand = new RelayCommand(GetAllPeople)); }
    }

    private void GetAllPeople()
    {
      SearchText = string.Empty;
      Search();
    }
    #endregion

    #endregion
  }

  /// <summary>
  /// Class used for message passing about a Person entry that has been deleted
  /// </summary>
  internal class DeleteMessage
  {
    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteMessage"/> class.
    /// </summary>
    /// <param name="person">The Person entry that has been deleted</param>
    public DeleteMessage(Person person)
    {
      PersonToDelete = person;
    }
    #endregion

    #region Properties
    /// <summary>Gets or sets the Person entry being deleted.</summary>
    public Person PersonToDelete { get; set; }
    #endregion
  }
}

