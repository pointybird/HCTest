﻿//—————————————————————————————————————————————————————
// <copyright file=”PersonVM.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HCTest.PeopleSearch.DAL;
using HCTest.PeopleSearch.Models;
using HCTest.PeopleSearch.Views;
using Microsoft.Win32;

namespace HCTest.PeopleSearch.ViewModels
{
  /// <summary>
  /// ViewModel for the Person view.
  /// </summary>
  /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
  public class PersonVM : ViewModelBase
  {
    private IDataAccess m_DataAccess;

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="PersonVM"/> class.
    /// </summary>
    /// <param name="dataAccess">The data access provider to use</param>
    public PersonVM(IDataAccess dataAccess)
    {
      m_DataAccess = dataAccess;

      // Register to respond to certain messages
      Messenger.Default.Register<Person>(this, LoadPerson);
      Messenger.Default.Register<DeleteMessage>(this, UpdateUIForDeleted);
    }
    #endregion

    #region Properties
    private Person m_Person;
    /// <summary>Gets or sets the person currently being viewed</summary>
    public Person Person
    {
      get { return m_Person; }
      set
      {
        if (m_Person != value)
        {
          m_Person = value;
          RaisePropertyChanged("Person");
          EditMode = false;
        }
      }
    }

    private bool m_EditMode;
    /// <summary>
    /// Gets a value indicating whether or not the Person data is in an editable mode.
    /// </summary>
    public bool EditMode
    {
      get { return m_EditMode; }
      private set
      {
        if (m_EditMode != value)
        {
          m_EditMode = value;
          RaisePropertyChanged("EditMode");
        }
      }
    }

    /// <summary>Gets the image to display for this Person.</summary>
    public ImageSource Image
    {
      get
      {
        if (m_Person == null)
        {
          return null;
        }

        return ImageUtils.BitmapFromBytes(m_Person.Image);
      }
    }

    private Interest m_SelectedInterest;
    /// <summary>Gets or sets the currently selected interest from the interest list</summary>
    public Interest SelectedInterest
    {
      get { return m_SelectedInterest; }
      set
      {
        if (m_SelectedInterest != value)
        {
          m_SelectedInterest = value;
          RaisePropertyChanged("SelectedInterest");
        }
      }
    }

    /// <summary>Gets the interests for this Person</summary>
    public ObservableCollection<Interest> Interests
    {
      get
      {
        return m_Person == null ? null : new ObservableCollection<Interest>(m_Person.Interests);
      }
    }
    #endregion

    #region Message Handling
    /// <summary>Loads info for a person and updates the view</summary>
    /// <param name="person">The person to load info for</param>
    private void LoadPerson(Person person)
    {
      if (Person != null)
      {
        // If a record is already loaded and has changes, ask to save it before new one is loaded
        if (Person.Id == 0 || m_DataAccess.HasChanges())
        {
            ConfirmChanges();
        }
      }

      // Update the UI
      UpdateForNewPerson(person);
    }

    /// <summary>Updates the UI for data from a new Person entry</summary>
    /// <param name="person">The person info is being loaded for</param>
    internal void UpdateForNewPerson(Person person)
    {
      Person = person;
      if (Person != null)
      {
        if (Person.Interests == null)
        {
          Person.Interests = new List<Interest>();
        }
      }

      RaisePropertyChanged(string.Empty);
    }

    /// <summary>Clear the view if the person just deleted is the same as the person whose info is being viewed</summary>
    /// <param name="msg">Message containing the person being deleted</param>
    private void UpdateUIForDeleted(DeleteMessage msg)
    {
      if (Person != null && msg.PersonToDelete == Person)
      {
        UpdateForNewPerson(null);
      }
    }
    #endregion

    #region Methods
    /// <summary>Asks the user to save any unsaved changes before loading a new Person entry</summary>
    private void ConfirmChanges()
    {
      if (MessageBox.Show("Do you wish to save the changes?", string.Empty, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        m_DataAccess.SavePerson(Person);
      }
      else
      {
        m_DataAccess.RevertChanges();
      }
    }
    #endregion

    #region Commands

    #region Edit command
    private ICommand m_EditCommand;
    /// <summary>Command to turn on edit mode to allow editing of person's info</summary>
    public ICommand EditCommand
    {
      get
      {
        return m_EditCommand ?? (m_EditCommand = new RelayCommand(EditPerson, CanEditPerson));
      }
    }

    private bool CanEditPerson()
    {
      return Person != null;
    }

    internal void EditPerson()
    {
      EditMode = true;
    }
    #endregion

    #region New command
    private ICommand m_NewCommand;
    /// <summary>Command to create and begin editing a new Person object</summary>
    public ICommand NewCommand
    {
      get
      {
        return m_NewCommand ?? (m_NewCommand = new RelayCommand(NewPerson));
      }
    }

    private void NewPerson()
    {
      Messenger.Default.Send(new Person()); // Trigger registered listener to update view for the new Person
      EditMode = true;
    }
    #endregion

    #region Save command
    private ICommand m_SaveCommand;
    /// <summary>Command to manually save edited info for Person</summary>
    public ICommand SaveCommand
    {
      get { return m_SaveCommand ?? (m_SaveCommand = new RelayCommand(SavePerson, CanSavePerson)); }
    }

    private bool CanSavePerson()
    {
      return Person != null && (Person.Id == 0 || m_DataAccess.HasChanges());
    }

    private void SavePerson()
    {
      m_DataAccess.SavePerson(Person);
      EditMode = false;
    }
    #endregion

    #region Edit Picture command
    private ICommand m_EditPictureCommand;
    /// <summary>Command to bring up dialog to change picture associated with a Person</summary>
    public ICommand EditPictureCommand
    {
      get { return m_EditPictureCommand ?? (m_EditPictureCommand = new RelayCommand(EditPicture, CanEditPicture)); }
    }

    private bool CanEditPicture()
    {
      return EditMode;
    }

    private void EditPicture()
    {
      // Open dialog to browse for image files
      OpenFileDialog dlg = new OpenFileDialog()
      {
        DefaultExt = ".png",
        Filter = "Image files (*.jpg, *.jpeg, *.gif, *.tiff, *.png) | *.jpg; *.jpeg; *.gif; *.tiff; *.png"
      };

      var result = dlg.ShowDialog();
      if (result == true)
      {
        try
        {
          m_Person.Image = ImageUtils.ImageBytesFromUri(new Uri(dlg.FileName));

          RaisePropertyChanged("Image");
        }
        catch (Exception)
        {
          MessageBox.Show(string.Format("Unable to load {0}", dlg.FileName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
      }
    }
    #endregion

    #region Add Interest command
    private ICommand m_AddInterestCommand;
    /// <summary>Command to bring up dialog to add a new interest for a Person</summary>
    public ICommand AddInterestCommand
    {
      get
      {
        return m_AddInterestCommand ?? (m_AddInterestCommand = new RelayCommand(AddInterest, CanAddInterest));
      }
    }

    private bool CanAddInterest()
    {
      return EditMode;
    }

    private void AddInterest()
    {
      // Bring up dialog to select a new interest
      var dialog = new InterestDialog();
      if (dialog.ShowDialog() == true)
      {
        var interest = dialog.GetSelection();
        // Associate selection with Person if this is a new interest for them
        if (interest != null && Person.Interests.Find(i => i.Name == interest.Name) == null)
        {
          Person.Interests.Add(interest);
          RaisePropertyChanged("Interests");
          SelectedInterest = null;
        }
      }
    }
    #endregion

    #region DeleteInterestCommand
    private ICommand m_DeleteInterestCommand;
    /// <summary>Command to delete an interest from a Person</summary>
    public ICommand DeleteInterestCommand
    {
      get
      {
        return m_DeleteInterestCommand ?? (m_DeleteInterestCommand = new RelayCommand(DeleteInterest, CanDeleteInterest));
      }
    }

    private bool CanDeleteInterest()
    {
      return EditMode && SelectedInterest != null;
    }

    private void DeleteInterest()
    {
      Person.Interests.Remove(SelectedInterest);
      m_DataAccess.MarkChanged(Person);
      RaisePropertyChanged("Interests");
      SelectedInterest = null;
    }
    #endregion

    #endregion
  }
}
