﻿//—————————————————————————————————————————————————————
// <copyright file=”NotNullToVisibilityConverter.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HCTest.PeopleSearch.ViewModels.Converters
{
  /// <summary>
  /// Converter to map object nullness to a Visibility state
  /// </summary>
  /// <seealso cref="System.Windows.Data.IValueConverter" />
  public class NotNullToVisibilityConverter : IValueConverter
  {
    /// <summary>Converts a value to a Visibility state</summary>
    /// <param name="value">The value produced by the binding source.</param>
    /// <param name="targetType">The type of the binding target property.</param>
    /// <param name="parameter">The converter parameter to use.</param>
    /// <param name="culture">The culture to use in the converter.</param>
    /// <returns>
    /// A Visibility state of Visible if the object is null, else Collapsed for any non-null value.
    /// </returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
      {
        return Visibility.Visible;
      }

      return Visibility.Collapsed;
    }

    /// <summary>Unimplemented as no sensible default behavior is possible.</summary>
    /// <exception cref="System.NotImplementedException"></exception>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
