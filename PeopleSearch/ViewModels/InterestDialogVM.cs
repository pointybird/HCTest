﻿//—————————————————————————————————————————————————————
// <copyright file=”InterestDialogVM.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using HCTest.PeopleSearch.DAL;
using HCTest.PeopleSearch.Models;

namespace HCTest.PeopleSearch.ViewModels
{
  /// <summary>
  /// ViewModel for the InterestDialog view
  /// </summary>
  /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
  public class InterestDialogVM : ViewModelBase
  {
    private IDataAccess m_DataAccess;

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="InterestDialogVM"/> class.
    /// </summary>
    /// <param name="dataAccess">The data access provider to use</param>
    public InterestDialogVM(IDataAccess dataAccess)
    {
      m_DataAccess = dataAccess;
    }
    #endregion

    #region Properties
    private ObservableCollection<Interest> m_Interests;
    /// <summary>Collection of all interests available to choose from</summary>
    public ObservableCollection<Interest> Interests
    {
      get
      {
        if (m_Interests == null)
        {
          m_Interests = new ObservableCollection<Interest>(m_DataAccess.GetInterests().OrderBy(i => i.Name));
          SelectedInterest = m_Interests.FirstOrDefault(); // Auto-select first in list, if possible
        }
        return m_Interests;
      }
    }

    private Interest m_SelectedInterest;
    /// <summary>Gets or sets the interest currently selected by user</summary>
    public Interest SelectedInterest
    {
      get { return m_SelectedInterest; }
      set
      {
        if (m_SelectedInterest != value)
        {
          m_SelectedInterest = value;
          RaisePropertyChanged("SelectedInterest");
        }
      }
    }
    #endregion
  }
}
