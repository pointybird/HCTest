﻿//—————————————————————————————————————————————————————
// <copyright file=”ViewModelLocator.cs”>
//     Copyright (c) Steven Clark. All rights reserved.
//     Written for Health Catalyst
// </copyright>
//———————————————————————–—————————————————————————————

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using HCTest.PeopleSearch.DAL;
using Microsoft.Practices.ServiceLocation;

namespace HCTest.PeopleSearch.ViewModels
{
  /// <summary>
  /// This class contains static references to all the view models in the
  /// application and provides an entry point for the bindings.
  /// </summary>
  public class ViewModelLocator
  {
    #region Constructor
    /// <summary>
    /// Initializes a new instance of the ViewModelLocator class.
    /// </summary>
    public ViewModelLocator()
    {
      ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

      if (ViewModelBase.IsInDesignModeStatic)
      {
        // Create design time view services and models
        //SimpleIoc.Default.Register<IDataAccess, DataAccess>();
      }
      else
      {
        // Create run time view services and models
        SimpleIoc.Default.Register<IDataAccess, DataAccess>();
      }

      // Register ViewModels
      SimpleIoc.Default.Register<SearchVM>();
      SimpleIoc.Default.Register<PersonVM>();
      SimpleIoc.Default.Register<InterestDialogVM>();
    }
    #endregion

    #region Properties
    /// <summary>Gets the registered data access provider</summary>
    public IDataAccess DataAccess
    {
      get
      {
        return ServiceLocator.Current.GetInstance<IDataAccess>();
      }
    }

    /// <summary>Gets the registered Search ViewModel</summary>
    public SearchVM SearchVM
    {
      get { return SimpleIoc.Default.GetInstance<SearchVM>(); }
    }

    /// <summary>Gets the registered Person ViewModel</summary>
    public PersonVM PersonVM
    {
      get { return SimpleIoc.Default.GetInstance<PersonVM>(); }
    }

    /// <summary>Gets the registered InterestDialog ViewModel</summary>
    public InterestDialogVM InterestDialogVM
    {
      get { return SimpleIoc.Default.GetInstance<InterestDialogVM>(); }
    }
    #endregion

    #region Methods
    /// <summary>Cleanup when done with locator</summary>
    public static void Cleanup()
    {
      if (!ViewModelBase.IsInDesignModeStatic)
      {
        SimpleIoc.Default.Unregister<IDataAccess>();
      }

      SimpleIoc.Default.Unregister<SearchVM>();
      SimpleIoc.Default.Unregister<PersonVM>();
      SimpleIoc.Default.Unregister<InterestDialogVM>();
    }
    #endregion
  }
}
